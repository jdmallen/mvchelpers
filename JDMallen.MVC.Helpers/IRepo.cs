﻿namespace JDMallen.MVC.Helpers {
    public interface IRepo {
        void SaveChanges();
    }

    public interface IRepo<out TDestType> : IRepo
        where TDestType : IEnt {
    }

    public interface IRepo<out TDestType, in TPK> : IRepo
        where TDestType : IEnt<TPK> {
        TDestType Get(TPK id);
    }
}
