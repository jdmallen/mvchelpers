﻿using System;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace JDMallen.MVC.Helpers {
    public class JsonNetResult : JsonResult {
        public override void ExecuteResult(ControllerContext context) {
            if (context == null) {
                throw new ArgumentNullException(nameof(context));
            }

            HttpResponseBase response = context.HttpContext.Response;

            response.ContentType = !string.IsNullOrEmpty(ContentType)
                ? ContentType
                : "application/json";

            if (ContentEncoding != null) {
                response.ContentEncoding = ContentEncoding;
            }

            string serializedObject = JsonConvert.SerializeObject(Data,
                Formatting.Indented);
            response.Write(serializedObject);
        }
    }
}
