﻿namespace JDMallen.MVC.Helpers {
    public interface IContext {
        void SaveChanges();
    }
}
