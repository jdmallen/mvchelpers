﻿using System;

namespace JDMallen.MVC.Helpers {
    public abstract class Ent : IEnt {}

    public abstract class Ent<TPK> : Ent, IEnt<TPK> {
        public TPK ID { get; protected set; }

        protected Ent() {}

        protected Ent(TPK id) {
            if ((object) id == null)
                throw new ArgumentNullException(nameof(id));
            this.ID = id;
        }
    }
}
