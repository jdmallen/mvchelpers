﻿namespace JDMallen.MVC.Helpers {
    public interface IEnt {}

    public interface IEnt<out TPK> : IEnt {
        TPK ID { get; }
    }
}
