﻿using System;
using System.Linq;
using System.Reflection;
using AutoMapper;
using AutoMapper.Internal;

namespace JDMallen.MVC.Helpers {
    public static class AutoMapperProfileLoader {
        public static void Load(params string[] assemblyNames) {
            assemblyNames.Select(Assembly.Load)
                .SelectMany(assembly => assembly.GetTypes())
                .Where(type => type.IsSubclassOf(typeof (Profile)))
                .Select(type => (Profile) Activator.CreateInstance(type))
                .Each(Mapper.AddProfile);
        }
    }
}
