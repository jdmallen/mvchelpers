﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using AutoMapper.QueryableExtensions;

namespace JDMallen.MVC.Helpers {
    public abstract class Repo<TContext> : IRepo
        where TContext : class, IContext {
        protected TContext Context { get; }

        protected Repo(TContext context) {
            if (context == null)
                throw new ArgumentNullException(nameof(context));
            Context = context;
        }

        public virtual void SaveChanges() {
            Context.SaveChanges();
        }

        protected TDestType Map<TDestType>(object ent) {
            return Mapper.Map<TDestType>(ent);
        }

        protected IEnumerable<TDestType> Map<TDestType>(
            IEnumerable<object> entities) {
            return Mapper.Map<IEnumerable<TDestType>>(entities);
        }

        protected IQueryable<TDestType> Project<T, TDestType>(
            IQueryable<T> entities) {
            return entities.ProjectTo<TDestType>();
        }
    }

    public abstract class Repo<TContext, TDestType> : Repo<TContext>
        where TContext : class, IContext where TDestType : IEnt {

        protected Repo(TContext context) : base(context) {}

        protected TDestType Map(object ent) {
            return Mapper.Map<TDestType>(ent);
        }

        protected IEnumerable<TDestType> Map(IEnumerable<object> entities) {
            return Mapper.Map<IEnumerable<TDestType>>(entities);
        }

        protected IQueryable<TDestType> Project<T>(IQueryable<T> entities) {
            return entities.ProjectTo<TDestType>();
        }
    }

    public abstract class Repo<TContext, TDestType, TPK> :
        Repo<TContext, TDestType>, IRepo<TDestType, TPK>
        where TContext : class, IContext where TDestType : IEnt<TPK> {

        protected Repo(TContext context) : base(context) {}

        public abstract TDestType Get(TPK id);
    }
}
