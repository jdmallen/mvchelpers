﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Web.Http.Dependencies;
using Ninject;
using Ninject.Syntax;
using IDependencyResolver = System.Web.Http.Dependencies.IDependencyResolver;

namespace JDMallen.MVC.Helpers {
    /// <summary>
    /// Everything is from here:
    /// http://blog.developers.ba/simple-way-share-container-mvc-web-api/
    /// </summary>
    public class NinjectDependencyResolver : NinjectDependencyScope, IDependencyResolver, System.Web.Mvc.IDependencyResolver {
        private readonly IKernel kernel;

        public NinjectDependencyResolver(IKernel kernel)
            : base(kernel) {
            this.kernel = kernel;
        }

        public IDependencyScope BeginScope() {
            return new NinjectDependencyScope(this.kernel.BeginBlock());
        }
    }

    /// <summary>
    /// Everything is from here:
    /// http://blog.developers.ba/simple-way-share-container-mvc-web-api/
    /// </summary>
    public class NinjectDependencyScope : IDependencyScope {
        private IResolutionRoot resolver;

        internal NinjectDependencyScope(IResolutionRoot resolver) {
            Contract.Assert(resolver != null);

            this.resolver = resolver;
        }

        public void Dispose() {
            var disposable = this.resolver as IDisposable;
            if (disposable != null) {
                disposable.Dispose();
            }

            this.resolver = null;
        }

        public object GetService(Type serviceType) {
            if (this.resolver == null) {
                throw new ObjectDisposedException("this", "This scope has already been disposed");
            }

            return this.resolver.TryGet(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType) {
            if (this.resolver == null) {
                throw new ObjectDisposedException("this", "This scope has already been disposed");
            }

            return this.resolver.GetAll(serviceType);
        }
    }
}
