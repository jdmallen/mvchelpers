﻿using System.Reflection;
using Dapper;

namespace JDMallen.MVC.Helpers {
    public static class Extensions {
        public static DynamicParameters ToParams<T>(this T obj) {
            DynamicParameters dynParams = new DynamicParameters();
            PropertyInfo[] props = typeof (T).GetProperties();
            foreach (PropertyInfo prop in props) {
                dynParams.Add(prop.Name, prop.GetValue(obj));
            }
            return dynParams;
        }
    }
}
